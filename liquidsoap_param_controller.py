#!/usr/bin/env python3
"""
Simple widget to control volume on a liquidsoap stream
"""

import telnetlib
import sys

from prompt_toolkit.application import Application
from prompt_toolkit.application.current import get_app
from prompt_toolkit.key_binding import KeyBindings
from prompt_toolkit.key_binding.bindings.focus import focus_next, focus_previous
from prompt_toolkit.layout.containers import Float, HSplit, VSplit
from prompt_toolkit.layout.dimension import D
from prompt_toolkit.layout.layout import Layout
from prompt_toolkit.styles import Style
from prompt_toolkit.widgets import (
  Box,
  Button,
  Checkbox,
  Dialog,
  Frame,
  Label,
  MenuContainer,
  MenuItem,
  ProgressBar,
  RadioList,
  TextArea,
)

# hardcoded telnet variables
telnet_host = '127.0.0.1'
telnet_port = 1234

def print_conn_err():
  print('Connection failed:\n  telnet ' + telnet_host + ' ' + str(telnet_port))

# start liquidsoap telnet interface
try:
  tn = telnetlib.Telnet(telnet_host, telnet_port)
except:
  print_conn_err()
  sys.exit(1)
tn.write(b"var.get volume" + b"\n")
tn_input=tn.read_some().decode('ascii').split('\n')[0]
init_volume=int(float(tn_input))
volume_button=Button(text=init_volume)

# write new volume value via telnet
def update_volume(value):
  try:
    # encode var to ascii -> src https://docs.python.org/3/library/telnetlib.html
    mybytes = b"var.set volume=" + str(volume_button.text).encode('ascii') + b"\n"
    tn.write( mybytes )
  except:
    get_app().exit(result=False)

# global app key bindings
# https://python-prompt-toolkit.readthedocs.io/en/master/pages/advanced_topics/key_bindings.html
bindings = KeyBindings()

@bindings.add("c-c")
@bindings.add("q")
@bindings.add("escape")
def _(event):
  " Exit the user interface. "
  event.app.exit()

# volume control (up/down)

@bindings.add('up')
def _(event):
  # correct way to assign it, thanks https://github.com/prompt-toolkit/python-prompt-toolkit/issues/677
  volume_button.text = volume_button.text + 1
  update_volume(volume_button.text)

@bindings.add('down')
def _(event):
  if ( int(volume_button.text) > 0 ):
    volume_button.text = volume_button.text - 1
    update_volume(volume_button.text)

@bindings.add('0')
@bindings.add('1')
@bindings.add('2')
@bindings.add('3')
@bindings.add('4')
@bindings.add('5')
@bindings.add('6')
@bindings.add('7')
@bindings.add('8')
@bindings.add('9')
def _(event):
  volume_button.text = int(event.data)
  update_volume(volume_button.text)

root_container = Box(
  Frame(body=volume_button, title="Volume"),
)

application = Application(
  layout=Layout(root_container, focused_element=volume_button),
  key_bindings=bindings,
  mouse_support=True,
  full_screen=True,
)

def run():
  app = application.run()
  if app == False:
    print_conn_err()

if __name__ == "__main__":
  run()
