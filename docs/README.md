## mermaid diagram

rhombus are icecast sources

```mermaid
flowchart LR
streamer -- static\nicecast\ncredentials -->live1.mp3{live1.mp3}
streamer -- dynamic\nliquidsoap\ncredentials -->live2.liq
live1.mp3-- priority 1 -->main.liq
live2.liq-->live2.mp3{live2.mp3}-- priority 2 -->main.liq
wordpress_schedule_API-- priority 1-->schedule.liq
playlist-- priority 2-->schedule.liq
schedule.liq-->schedule.mp3{schedule.mp3}-- priority 3 -->main.liq
blank-- priority 4 -->main.liq
main.liq --> main.mp3{main.mp3}
main.liq --> 2day_radio_archive
main.liq -- only\n live\ncontent --> live.mp3{live.mp3}
2day_radio_archive --> nextcloud_folder
main.mp3 --> wp_web_player
```
